import java.util.Scanner;

public class AsciiWrapper {


    public int toAscii(char character) {

        return character;
    }

    public char toCharacter(int value) {

        return (char)value;
    }


    public static void main(String[] args) {
	// write your code here

        Scanner input = new Scanner(System.in) ;

        char anyCharacter = input.next().charAt(0);


        AsciiWrapper obj = new AsciiWrapper() ;
        System.out.println(obj.toAscii(anyCharacter));



        int anyValue = input.nextInt() ;
        AsciiWrapper obj2 = new AsciiWrapper() ;
        System.out.println(obj2.toCharacter(anyValue));


    }
}
